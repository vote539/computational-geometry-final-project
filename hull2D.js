// Make an instance of two and place it on the page.
var two = new Two({
	width: 400,
	height: 400
}).appendTo(document.getElementById("convex-hull-holder"));

// Helper functions
function showPoint(p, radius){
	radius = radius || 2;
	if(!p) return;
	two.makeCircle(p.x, p.y, radius);
}
function showPolygon(points){
	// clone the points first
	return two.makePolygon(points.map(function(p){
		return p.clone();
	}));
}

function compute2DHull(n, useChan, m){
	two.clear();

	// Compute random points
	var points = [];
	for(var i=0; i<n; i++){
		points.push(new Two.Anchor(
			Math.random()*400,
			Math.random()*400
		));
	}

	// Compute the convex hull
	var start = new Date();
	var hull;
	if(useChan){
		hull = util.hull.chan(points, m, {
			leftmost: util.leftmost.linear,
			tangent: util.tangent.linear,
			innerHull: util.hull.grahamsScan
		}, {
			hullCallback: function(innerHull){
				if(!window.noRender)
					showPolygon(innerHull).noFill();
			}
		});
	}else{
		hull = util.hull.grahamsScan(points);
	}
	var end = new Date();

	console.log("Time Data 2D:\n\n"
		+ "Number of Points: " + n + "\n"
		+ "Algorithm: " + (useChan?"Chan":"Graham") + "\n"
		+ (useChan?"Value of m: " + m + "\n":"")
		+ "Number of Edges in Hull: " + hull.length + "\n"
		+ "Runtime: " + (end.getTime() - start.getTime()) + " ms");

	// Display the points
	if(!window.noRender){
		points.forEach(function(p){
			showPoint(p);
		});
		showPolygon(hull).noFill().stroke = "red";
		two.update();
	}
}

function recompute2DHull(){
	var n = parseInt($("input[name=num]", "#hull-form").val());
	var alg = $("input[name=hullalg]:checked", "#hull-form").val();
	var m = parseInt($("input[name=m]", "#hull-form").val());
	compute2DHull(n, alg === "chan", m);
}

compute2DHull(50, true, 6);
