<!DOCTYPE html>
<html>
<head>
	<title>Computational Geometry Final Project</title>
	<meta name="author" content="Shane Carr" />
	<style type="text/css">
	#land {
		fill: #b8b8b8;
		stroke: #fff;
		stroke-width: .5px;
		stroke-linejoin: round;
	}
	.graticule {
		fill: none;
		stroke: #000;
		stroke-opacity: .3;
		stroke-width: .5px;
	}
	circle.mouse {
		fill: none;
		stroke: #000;
		pointer-events: all;
	}
	.arcs {
		fill: none;
		stroke: red;
		stroke-width: .5px;
	}
	#svg-holder{
	}
	#cities-holder{
		width: 300px;
	}
	#predefined-sets-holder{
		width: 220px;
	}
	#many-random-cities-holder{
		width: 220px;
	}
	#cities-submit-btn{
	}
	body{
		font: 12pt/18pt Helvetica, Arial, sans-serif;
	}
	#controls{
		display: none;
	}
	#convex-hull-holder{
		position: relative;
		width: 400px;
		height: 400px;
	}
	figure {
		display: block;
		width: 95%;
	}
	figure > img {
		display: block;
		width: inherit;
	}
	figure > figcaption {
		display: block;
		font-style: italic;
		font-size: 0.9em;
		line-height: 1em;
		width: inherit;
	}
	</style>
	<link rel="stylesheet" type="text/css" href="vendor/chosen_v1.1.0/chosen.min.css" />
</head>
<body>
	<h1>Computational Geometry Final Project</h1>
	<p>Shane Carr &mdash; May 2014</p>
	<p>Summary: In class we have discussed extensively about two-dimensional convex hulls, not not as much in three dimensions.  In this project I have created two interactive demos, one for a three-dimensional convex hull and one for a two-dimensional convex hull, and I have compared their implementation and runtimes.</p>
	<section>
		<h2>Delaunay on a Sphere: Interactive Demo</h2>
		<div id="svg-holder"></div>
		<div id="controls">
			<h3>Pick Your Cities</h3>
			<form id="cities-form">
				<div id="cities-holder">
					<select id="cities-select" multiple class="chosen-select" data-placeholder="Select Cities"></select>
				</div>
				<input type="submit" value="Update" id="cities-submit-btn" />
				<div id="predefined-sets-holder">Random Sets of Cities with Pop > 1 Million: </div>
				<div id="many-random-cities-holder">Randomly choose approximately this many cities: </div>
			</form>
			<script>
			document.getElementById("cities-form").addEventListener("submit", function(event){
				event.preventDefault();
				recomputeSphere();
			}, false);
			</script>
		</div>
	</section>
	<section>
		<h2>Convex Hull in 2D: Interactive Demo</h2>
		<div id="convex-hull-holder"></div>
		<form id="hull-form">
			<p>
				<label>Number of Points: <input type="number" min="3" value="50" name="num" /></label>
			</p>
			<p>
				Algorithm: <label><input type="radio" name="hullalg" value="graham" />Graham's Scan</label> <label><input type="radio" name="hullalg" value="chan" checked />Chan's Algorithm</label>
			</p>
			<p>
				<label>Value of <em>m</em> (for Chan): <input type="number" min="3" value="6" name="m" /></label>
			</p>
			<p>
				<input type="submit" value="Update" id="cities-submit-btn" />
			</p>
		</form>
		<script>
		document.getElementById("hull-form").addEventListener("submit", function(event){
			event.preventDefault();
			recompute2DHull();
		}, false);
		</script>
	</section>
	<section>
		<h2>Discussion</h2>
		<p>There are two widgets in my project: one computes the convex hull in 2D, and the other computes the convex hull in 3D.  My goal is to put them side-by-side for a comparison.</p>
		<p>First I will talk about the sphere version, then I will talk about the 2D version.</p>
		<h3>Delaunay of a Sphere</h3>
		<p>It has been known for several decades that there exists a correlation between the Delaunay triangulation on a sphere and the convex hull of that sphere.  In fact, the two structures are identical.  This lemma was the basis for K.Q. Brown's original argument that it was possible to compute both 2D and 3D voronoi diagrams in O(n log n) time (see Na et al.).</p>
		<p>The algorithm I use for computing the convex hull / Delaunay triangulation for a sphere is similar to the one documented by Eilberg.  It is essentially the gift-wrapping algorithm in 3D.  Given an edge of a Delaunay triangle, one can compute the angle between the plane formed by that triangle and the plane formed by that edge with each other point in space.  The new triangle with the smallest angle is added to the triangulation, and the algorithm continues until all edges have been processed.</p>
		<p>The code, which is commented for readability, is written in JavaScript.  You can find it at the following URLs.  All of the following code is custom:</p>
		<ul>
			<li><a href="sphere.js" target="_blank">sphere.js</a> - generates the point sets, calls <em>util.js</em> to compute the Delaunay, and paints it onto the sphere using the D3 library (the "controller").</li>
			<li><a href="util.js" target="_blank">util.js</a> - contains the actual Delaunay algorithm as well as the algorithms used in the 2D application (the "model").</li>
		</ul>
		<p>One way that my algorithm differs from Eilberg's is that I always keep one triangular face encircling Antarctica on the sphere.  Eilberg's algorithm incorrectly assumes that any three southernmost points form a Delaunay triangle.  Such a face can be guaranteed only if the plane corresponding to the face is normal to the <em>z</em> axis.  My Antarctic face satisfies this requirement.</p>
		<figure>
			<img src="images/faces_to_points.png" />
			<figcaption>
				Same trials as above, but showing the relationship between the number of points and the number of faces.  There appears to be an analytical relationship between the two, where F=3P-2.  This is consistent with Euler's theorem, with C=1 and F=1+3E/4.
			</figcaption>
		</figure>
		<p>The expected runtime for a gift-wrapping algorithm is O(nh).  As shown in the above figure, the number of faces in the Delaunay triangulation drows linearly with the number of points.  In other words, for a sphere, every point must be on the convex hull.  We therefore expect the algorithm to be O(n<sup>2</sup>).</p>
		<figure>
			<img src="images/delaunay_time.png" />
			<figcaption>
				Relationship between runtime and number of points.  Shown are 16 trials with different numbers of points on the sphere.  A polynomial relationship is evident.
			</figcaption>
		</figure>
		<p>The above figure is consistent with our expectation of polynomial runtime, and the correlation is strong.  There is a very high constant factor; even for quadratic algorithms, we typically need to exceed 10,000 data points before we notice a performance hit, but in this case 10 seconds of runtime is exceeded with only 100 points on the sphere.  This may be due in part to the JavaScript engine being used to run the code, in addition to the higher number of operations required when computing 3D instead of 2D.</p>
		<p>The most difficult and time-consuming aspect of this project was figuring out the vector algebra.  Choosing the correct cross-products and dot-products is easier on a Calc III exam than it is in code.  Most of the "bugs" in the code arose from faulty vector logic.  All known errors in the vector algebra have now been fixed.</p>
		<h3>Convex Hulls in 2D</h3>
		<p>Since we are dealing with convex hulls in 3D, it is worthwhile having a 2D analog for comparison.</p>
		<figure>
			<img src="images/chan_time.png" />
			<figcaption>
				A semilog plot of the runtime of Graham's Scan and Chan's algorithm.  Note how runtime grows relatively linearly, consistent with a O(nh) or O(n log h) runtime.  Chan's algorithm seems to be a constant factor faster than full Gram's scan.
			</figcaption>
		</figure>
		<p>The above figure shows the runtime of the 2D convex hull algorithm with different number of points.  Notice the semilog scale.  Chan's algorithm is documented to run in O(n log h) time.  For all samples, the number of points on the convex hull is between 18 and 42, so we can say that <em>h</em> is relatively constant, which would imply a O(n) runtime for Chan's algorithm.  This is consistent with the data collected above.</p>
		<p>We expect for Graham's scan to be O(n log n) in all cases.  Although Graham's scan is slower than Chan's algorithm by a constant factor, it does not demonstrate the O(n log n) runtime we expect, at least up to 1 million points.  The reasons behind this are unclear.</p>
		<p>When running the simulation above, the bottleneck in runtime is the canvas render, <em>not</em> the actual hull computation.  To test the hull computation for larger numbers than the canvas render can support, type the following into hte JavaScript console: <kbd>window.noRender = true</kbd>.  You can view the runtime in the JavaScript console output, for both the 2D and the 3D case.</p>
		<p>Note that the 2D version runs in well under 10 seconds for 1 million random points.  This is <em>significantly</em> faster than the gift wrapping algorithm on the sphere.  This brings us to the position that we hope to investigate alternative algorithms for the 3D convex hull.</p>
		<h2>Known Issues</h2>
		<p>For very small numbers of cities on the sphere, or if all cities are clustered together in one hemisphere, the plot appears to show intersecting triangle lines.  This is actually the result of the 3D projection library always assuming the shortest arc when connecting two points.  (On a sphere, there exists a longest-arc and a shortest-arc connecting points.)  This issue is not evident when cities are spread evenly across the globe.</p>
		<p>For certain values of <em>n</em> and <em>m</em> in the 2D convex hull demo, there is a JavaScript error that arises from the fact that there exist single-point inner hulls used for Chan's algorithm.  changing the value of <em>n</em> or <em>m</em> by one usually fixes the issue.</p>
		<h2>Future Work</h2>
		<p>In addition to addressing the known issues, future work should involve improving the runtime of the algorithm on the sphere using the following approahces:</p>
		<ol>
			<li>Using the approach documented by Na et al., project the sphere onto an appropriate plane, use an efficient 2D Delaunay algorithm to compute the tessellation, and then map the graph back onto thesphere.  We would expect this approach to take O(n log n) time.</li>
			<li>Using the approach documented by Chan, use the 3D version of Chan's algorithm to compute the convex hull of the sphere.  We would expect this approach to take O(n log h) = O(n log n), since h=O(n) for points on a sphere.</li>
		</ol>
		<h2>References</h2>
		<section style="margin-left:.5in;text-indent:-.5in">
			<p>
				T. M. Chan, &ldquo;Optimal output-sensitive convex hull algorithms in two and three dimensions,&rdquo; <i>Discrete &amp; Computational Geometry,</i> vol. 16, no. 4, pp. 361-368, 1996/04/01, 1996.
			</p>
			<p>
				E. Eilberg, &ldquo;Convex hull algorithms,&rdquo; <i>Student Scholarship</i>, 2004.<p>
			</p>
			<p>
				H.-S. Na, C.-N. Lee, and O. Cheong, &ldquo;Voronoi diagrams on the sphere,&rdquo; <i>Computational Geometry,</i> vol. 23, no. 2, pp. 183-194, 9//, 2002.<p>
			</p>
			<p>
				T. Switzer, &quot;2D Convex Hulls: Chan's Algorithm,&quot; 2010.
			</p>
		</section>
		<h2>Software Libraries and Resources</h2>
		<p>
			This project uses the following software libraries and resources.  We thank their respective creators and maintainers.
		</p>
		<ul>
			<li><a href="http://sylvester.jcoglan.com/">Sylvester</a> - vector algebra library for JavaScript, used for computing cross products and such</li>
			<li><a href="http://jquery.com/">jQuery</a> - document manipulation library for JavaScript</li>
			<li><a href="https://github.com/placemarker/jQuery-MD5">jQuery-MD5</a> - JavaScript library to compute the MD5 hash of an input string (used for creating deterministic random sets of points)</li>
			<li><a href="http://harvesthq.github.io/chosen/">Chosen</a> - library for the sexy city selector used in the sphere demo</li>
			<li><a href="http://jonobr1.github.io/two.js/">Two.js</a> - lightweight library for rendering points in the 2D plane (used for displaying the 2D convex hull)</li>
			<li><a href="http://d3js.org/">D3</a> - library used for rendering the 3D sphere</li>
			<li><a href="http://www.geonames.org/export/">GeoNames</a> - database of major cities in the world (used to populate the city selector)</li>
			<li><a href="http://www.naturalearthdata.com/">Natural Earth</a> - database of land topology on the Earth's surface (used to paint the continent background)</li>
		</ul>
		<p>
			Note that the actual algorithms were implemented by hand without the help of an external library.
		</p>
	</section>

	<!-- JavaScript canvas libraries -->
	<script src="vendor/two.js/build/two.js"></script>
	<script src="vendor/sylvester/sylvester.src.js"></script>
	<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
	<script src="http://d3js.org/queue.v1.min.js"></script>
	<script src="http://d3js.org/topojson.v1.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="vendor/chosen_v1.1.0/chosen.jquery.js"></script>
	<script src="vendor/jquery-md5/jquery.md5.js"></script>

	<!-- My Computational Geometry Algorithms -->
	<script src="util.js"></script>

	<!-- Runtime -->
	<script src="hull2D.js"></script>
	<script src="sphere.js"></script>

</body>
</html>