/* The globe projection is based off the example at
** http://techslides.com/demos/d3/d3-globe.html
**/

// Initial SVG setup
var width = 400,
    height = 400;
var svg = d3.select("#svg-holder").append("svg")
    .attr("width", width)
    .attr("height", height);

// Orthographic projection setup
var projection = d3.geo.orthographic()
    .translate([width / 2, height / 2])
    .scale(200)
    .rotate([98,-39])
    .clipAngle(90);
var path = d3.geo.path()
    .projection(projection);

// Display the graticule (lat/lon lines)
var line = svg.append("path")
    .datum(d3.geo.graticule())
    .attr("class", "graticule")
    .attr("d", path);

// Default features around antarctica so that we always have an initial triangle to start from.  Note that the southernmost city, Ushuaia, is north of -60 degrees.
var defaultFeatures = [
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [0, -60]
    },
    properties: { name: "S-Africa" }
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [120, -60]
    },
    properties: { name: "S-Australia" }
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-120, -60]
    },
    properties: { name: "S-SouthAmerica" }
  }
]

// Function to compute and display the triangulation
var delaunayNode = svg.append("g");
function recomputeSphere(){
  var times = [];

  // Get the chosen cities from the select field
  var chosenCities = $("#cities-select").find(":selected").map(function(){
    var id = parseInt($(this).val());
    for(var i=0; i<cities.length; i++){
      if(cities[i].id === id){
        return cities[i];
      }
    }
    return null;
  }).toArray();

  // Convert to D3 "features"
  var chosenFeatures = chosenCities.map(function(city){
    return {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [
          city.longitude,
          city.latitude
        ]
      },
      properties: city
    };
  }).concat(defaultFeatures);

  // Clear the old triangulation if applicable and make a new element group
  delaunayNode.remove();
  delaunayNode = svg.append("g");

  times.push(new Date());

  // Display the cities
  delaunayNode.append("g")
      .attr("class", "points")
      .selectAll("text")
      .data(chosenFeatures)
      .enter()
      .append("path")
      .attr("class", "point")
      .attr("d", path);

  times.push(new Date());

  // Compute the triangulation (see util.js)
  var gws = window.gws = new util.GiftWrapSphere(chosenFeatures);

  times.push(new Date());

  // Display the triangulation
  var arcs = gws.edges.map(function(edge){
    return {
      "type": "Feature",
      "geometry": {
        "type": "LineString",
        "coordinates": edge.coordinates
      }
    }
  });
  delaunayNode.append("g")
      .attr("class","arcs")
      .selectAll("path")
      .data(arcs)
      .enter()
      .append("path")
      .attr("class","arc")
      .attr("d",path);

  times.push(new Date());

  var timeStr = "Time Data:\n\n";
  timeStr += "Number of Points: " + gws.spoints.length + "\n";
  timeStr += "Number of Faces in Convex Hull: " + gws.facets.length + "\n";
  timeStr += "Painting City Nodes: " + (times[1].getTime() - times[0].getTime()) + " ms\n";
  timeStr += "Computing Delaunay: " + (times[2].getTime() - times[1].getTime()) + " ms\n";
  timeStr += "Painting Arcs: " + (times[3].getTime() - times[2].getTime()) + " ms\n";
  console.log(timeStr);
}

// Load the external data
var world, cities, countries;
queue()
  .defer(d3.json, "vendor/data/world-110m.json")
  .defer(d3.csv, "vendor/data/cities50000.csv", function(d){
    // id,name,asciiname,latitude,longitude,country_code,population,elevation,timezone,last_modified
    d.id = parseInt(d.id);
    d.latitude = parseFloat(d.latitude);
    d.longitude = parseFloat(d.longitude);
    d.population = parseInt(d.population);
    d.elevation = parseInt(d.elevation);
    return d;
  })
  .defer(d3.csv,  "vendor/data/countries.csv", function(d){
    // ISO,ISO3,ISONumeric,fips,Country,Capital,Area,Population,Continent,tld,CurrencyCode,CurrencyName,Languages,geonameid,neighbours,EquivalentFipsCode
    d.geonameid = parseInt(d.geonameid);
    d.Area = parseInt(d.Area);
    d.Population = parseInt(d.Population);
    d.ISONumeric = parseInt(d.ISONumeric);
    return d;
  })
  .await(function(error, _world, _cities, _countries) {
  world = _world;
  cities = _cities;
  countries = _countries;

  // Display the map data
  svg.append("path")
      .datum(topojson.feature(world, world.objects.land))
      .attr("id", "land")
      .attr("d", path);

  // Put the cities in the select list and make sixteen predefined sets of cities
  var citySets = [];
  $("#cities-select").append(cities.map(function(city){
    if(city.population > 1000000){
      var letter = $.md5(city.asciiname)[0];
      if(!citySets[letter]){
        citySets[letter] = [];
      }
      citySets[letter].push(city);
    }
    
    // innerHTML is faster than the DOM method for creating the nodes
    return '<option value="'+city.id+'">'+city.name+'</option>';
  }).join("\n")).chosen({ width: "100%" });
  function selectCitySet(set){
    var setIds = set.map(function(city){
      return city.id;
    });
    return function(){
      $("#cities-select").children().each(function(){
        var myId = parseInt($(this).val());
        $(this).attr("selected", setIds.indexOf(myId) !== -1);
      });
      $("#cities-select").trigger("chosen:updated");
    }
  }
  for(var letter in citySets){
    if(!citySets.hasOwnProperty(letter)) continue;
    var btn = $("<a href='javascript:null'>");
    btn.append(letter);
    btn.click(selectCitySet(citySets[letter]));
    $("#predefined-sets-holder").append(btn);
    $("#predefined-sets-holder").append(" ");
  }
  function selectManyRandomCities(i){
    var rndBnd = i / cities.length;
    return function(){
      $("#cities-select").children().each(function(){
        $(this).attr("selected", Math.random() < rndBnd);
      });
      $("#cities-select").trigger("chosen:updated");
    }
  }
  for(var i=10; i<=150; i+=10){
    var btn = $("<a href='javascript:null'>");
    btn.append(i);
    btn.click(selectManyRandomCities(i));
    $("#many-random-cities-holder").append(btn);
    $("#many-random-cities-holder").append(" ");
  }
  $("#controls").show();

  // Initial set
  selectCitySet(citySets["d"])();
  recomputeSphere();

  // Mouse Drag Rotation
  var lambda = d3.scale.linear()
      .domain([0, width])
      .range([-180, 180]);
  var phi = d3.scale.linear()
      .domain([0, height])
      .range([90, -90]);
  svg.append("circle")
      .datum({x: 0, y: 90})
      .attr("class", "mouse")
      .attr("cx", width / 2)
      .attr("cy", height / 2)
      .attr("r", projection.scale())
      .call(d3.behavior.drag()
        .origin(Object)
        .on("drag", function(d) {
          var p = d3.mouse(this);
          projection.rotate([lambda(p[0]), phi(p[1])]);
          svg.selectAll("path").attr("d", path)
        }));
});
