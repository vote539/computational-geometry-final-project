// Utility geometry functions

var util = {
	tangent: {
		linear: function(p, points){
			var len = points.length;
			for(var i=0; i<len; i++){
				var q = points[i];
				var ql = points[(i+len-1) % len];
				var qr = points[(i+len+1) % len];
				if (p===q) {
					return qr;
				}
				if(util.orient(p, q, ql) <= 0 && util.orient(p, q, qr) <= 0){
					return q;
				}
			}
			// The sample point is inside the hull
			return null;
		}
	},
	leftmost: {
		linear: function(points){
			var p = points[0];
			for(var i=1; i<points.length; i++){
				if(points[i].x < p.x)
					p = points[i];
			}
			return p;
		}
	},
	hull: {
		fromTriangle: function(inp){
			// Base case: catch if two or fewer points
			if(inp.length <= 2){
				return inp.slice(0);
			}
			// Constant time convex hull from three points
			var hull = [];
			hull.push(inp[0]);
			if(util.orient(inp[0], inp[1], inp[2]) < 0){
				hull.push(inp[1]);
				hull.push(inp[2]);
			}else{
				hull.push(inp[2]);
				hull.push(inp[1]);
			}
			return hull;
		},
		grahamsScan: function(inp){
			// Base case: catch if three or fewer points
			if(inp.length <= 3){
				return util.hull.fromTriangle(inp);
			}
			// Sort by x coordinate in O(n log n)
			var points = inp.slice(0);
			points.sort(function(a,b){
				return a.x - b.x;
			});
			// Compute the upper and lower hulls
			var hull = util.hull.grahamsScanWorker(points, false);
			var upper = util.hull.grahamsScanWorker(points, true);
			// Merge the hulls and return
			for(var i=upper.length-2; i>=1; i--){
				hull.push(upper[i]);
			}
			return hull;
		},
		grahamsScanWorker: function(sortedPoints, top){
			// Compute an upper or lower hull in O(n)
			var hull = sortedPoints.slice(0, 2);
			for(var i=2; i<sortedPoints.length; i++){
				hull.push(sortedPoints[i]);
				var orient;
				do {
					orient = util.orient(
						hull[hull.length - 3],
						hull[hull.length - 2],
						hull[hull.length - 1]
					);
					if (top && orient > 0) break;
					if (!top && orient < 0) break;
					hull.splice(hull.length - 2, 1);
				} while(hull.length >= 3);
			}
			return hull;
		},
		chan: function(inp, m, algs, options){
			options = options || {};
			var hulls = [];
			for(var i=m; i<=inp.length; i+=m){
				hulls.push(algs.innerHull(inp.slice(i-m, i)));
				if(options.hullCallback) options.hullCallback(hulls[hulls.length-1]);
			}
			// Last hull
			hulls.push(algs.innerHull(inp.slice(i-m, inp.length)));
			if(options.hullCallback) options.hullCallback(hulls[hulls.length-1]);
			return util.hull.chanWorker(hulls, algs);
		},
		chanWorker: function(hulls, algs){
			var n = hulls.length;
			var leftmostPoints = hulls.map(function(hull){
				return algs.leftmost(hull);
			});
			var lp = leftmostPoints[0];
			for(var i=1; i<n; i++){
				if(leftmostPoints[i].x < lp.x){
					lp = leftmostPoints[i];
				}
			}
			var outerHull = [new Two.Vector(lp.x, -999), lp];
			do{
				var p = outerHull[outerHull.length - 2];
				var q = outerHull[outerHull.length - 1];
				var rCandidates = hulls.map(function(hull){
					return algs.tangent(q, hull);
				});
				var bestTheta = 0, bestR = null;
				for(var i=0; i<n; i++){
					var r = rCandidates[i];
					var a = q.distanceTo(p), asq = a*a;
					var b = q.distanceTo(r), bsq = b*b;
					var c = p.distanceTo(r), csq = c*c;
					var theta = Math.acos((asq+bsq-csq)/(2*a*b));
					if(theta > bestTheta){
						bestR = r;
						bestTheta = theta;
					}
				}
				outerHull.push(bestR);
			}while(outerHull[outerHull.length-1] !== lp);
			return outerHull.slice(2);
		}
	},
	GiftWrapSphere: function(features){
		// Use a O(n^2) gift wrapping algorithm.
		// Inspired by Eilberg et al.

		// Define some data structures
		function SpherePoint(feature){
			this.name = feature.properties.name;
			this.feature = feature;
			this.coordinates = feature.geometry.coordinates;
			// Scoped shorthand variables
			var lonRad = this.coordinates[0]*Math.PI/180;
			var latRad = this.coordinates[1]*Math.PI/180;
			// Convert to cartesian coordinates.
			// Assume unit sphere with center at origin.
			this.x = Math.cos(latRad) * Math.cos(lonRad);
			this.y = Math.cos(latRad) * Math.sin(lonRad);
			this.z = Math.sin(latRad);
			this.vector = $V([this.x, this.y, this.z]);
			this.toString = function(){
				return "<SpherePoint "+this.name+" "+this.x+","+this.y+","+this.z+">";
			};
		};
		var SpherePointSorter = function(spA, spB){
			return spA.z - spB.z;
			//return spA.y - spB.y;
		};
		function Edge(spA, spB){
			this.spoints = [spA, spB];
			this.spoints.sort(SpherePointSorter);
			this.coordinates = this.spoints.map(function(spoint){
				return spoint.coordinates;
			});
			this.vector = spB.vector.subtract(spA.vector);
			this.equals = function(edge){
				return this.spoints[0] === edge.spoints[0] && this.spoints[1] === edge.spoints[1];
			};
			this.facetTo = function(spoint){
				return new Facet(
					this.spoints.concat(spoint),
					[
						this,
						makeEdge(spA, spoint),
						makeEdge(spB, spoint)
					]
				);
			};
			this.toString = function(){
				return "<Edge "+this.spoints[0].name+", "+this.spoints[1].name+">";
			}
		};
		function Facet(spoints, edges){
			this.spoints = spoints; // SpherePoints
			this.spoints.sort(SpherePointSorter);
			this.edges = edges;
			this.plane = $P(spoints[0].vector, spoints[1].vector, spoints[2].vector);
			// Flip the sign of the normal vector if the normal vector points toward the origin
			var projP = this.plane.pointClosestTo(this.plane.normal);
			if(projP.dot(this.plane.normal)<0){
				this.plane.normal = this.plane.normal.multiply(-1);
			}
			this.angleTo = function(facet){
				var normalsAngle = facet.plane.normal.angleFrom(this.plane.normal);
				var direction = this.edges[0].vector.dot(this.plane.normal.cross(facet.plane.normal));
				if(direction<0){
					return normalsAngle;
				}else{
					return 2*Math.PI - normalsAngle;
				}
			};
			this.isSpointAbove = function(spoint){
				return spoint.vector.subtract(spoints[0].vector).dot(this.plane.normal) > 0;
			};
			this.isRotationCorrect = function(facet){
				var spoint;
				for(var i=0; i<3; i++){
					if(facet.spoints.indexOf(this.spoints[i]) === -1){
						spoint = this.spoints[i];
						break;
					}
				}
				return !facet.isSpointAbove(spoint);
			};
			this.toString = function(){
				return "<Facet "+this.spoints[0].name+", "+this.spoints[1].name+", "+this.spoints[2].name+">";
			};
		};

		// Convert the raw data into SpherePoints and sort by z coordinate in O(n log n)
		var spoints = this.spoints = features.map(function(feature){
			return new SpherePoint(feature);
		});
		spoints.sort(SpherePointSorter);

		// Keep track of edges to prevent duplicates
		var masterEdgeStore = [];
		var makeEdge = function(spA, spB){
			var edge = new Edge(spA, spB);
			var i;
			for(i=0; i<masterEdgeStore.length; i++){
				if(masterEdgeStore[i].equals(edge)){
					edge = masterEdgeStore[i];
					break;
				}
			}
			if(i===masterEdgeStore.length){
				masterEdgeStore.push(edge);
			}
			return edge;
		}

		// The initial facet will contain the three southernmost cities.
		var allFacets = this.facets = [
			new Facet(
				spoints.slice(0,3),
				[
					makeEdge(spoints[0], spoints[1]),
					makeEdge(spoints[0], spoints[2]),
					makeEdge(spoints[1], spoints[2])
				]
			)
		];
		var allEdges = this.edges = [];
		function evaluateFacet(facet){
			// Note: I use a primitive for loop here instead of a forEach loop because a forEach loop adds an unnecessary level to the recursion stack.
			for(var i=0; i<3; i++){
				evaluateEdge(facet, facet.edges[i]);
			}
		};
		function evaluateEdge(facet, edge){
			if(allEdges.indexOf(edge) !== -1) return;
			allEdges.push(edge);
			var candidateFacets = [];
			spoints.forEach(function(spoint){
				if(facet.spoints.indexOf(spoint) === -1){
					candidateFacets.push(edge.facetTo(spoint));
				}
			});
			candidateFacets.sort(function(facetA, facetB){
				return facetA.angleTo(facet) - facetB.angleTo(facet);
			});
			var newFacet = facet.isRotationCorrect(candidateFacets[0])
				? candidateFacets[0]
				: candidateFacets[candidateFacets.length-1];
			//console.log(facet, facet.toString(), edge.toString(), newFacet.toString(), candidateFacets.map(function(_facet){ return _facet.toString()+" ("+_facet.angleTo(facet)+")"; }));
			allFacets.push(newFacet);
			evaluateFacet(newFacet);
		};

		// Start the algorithm
		var initFacet = this.facets[0];
		evaluateFacet(initFacet);
	},
	orient: function(p, q, r){
		var a = 1, b = p.x, c = p.y,
			d = 1, e = q.x, f = q.y,
			g = 1, h = r.x, i = r.y;
		return a*(e*i-f*h)-b*(d*i-f*g)+c*(d*h-e*g);
	}
};
